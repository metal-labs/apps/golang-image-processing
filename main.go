package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"sync"
)

func downloadImage(url string, wg *sync.WaitGroup) {
	defer wg.Done()

	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	defer resp.Body.Close()

	fileName := "downloaded_" + url[strings.LastIndex(url, "/")+1:]
	file, err := os.Create(fileName)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	defer file.Close()

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	fmt.Printf("Downloaded: %s -> %s\n", url, fileName)
}

func main() {
	images := []string{
		"https://cdn.pixabay.com/photo/2016/10/18/15/22/sword-1750506_1280.jpg",
		"https://cdn.pixabay.com/photo/2015/11/23/14/06/sword-1058402_1280.jpg",
	}

	var wg sync.WaitGroup

	for _, url := range images {
		wg.Add(1)
		go downloadImage(url, &wg)
	}

	wg.Wait()

	fmt.Println("All downloads finished!")
}
